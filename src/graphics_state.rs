extern crate sfml;
use graphics_state::sfml::window::{Event, Style};
use graphics_state::sfml::graphics::RenderWindow;

pub struct GraphicsState {
	window: RenderWindow,
}

impl GraphicsState {
	pub fn new(w: u32, h: u32) -> GraphicsState {
		let mut window = RenderWindow::new(
			(w,h),
			"CHIP-8 Emulator",
			Style::CLOSE,
			&Default::default()
		);
		window.set_framerate_limit(500);
		GraphicsState { window: window }
	}

	pub fn render_loop<F>(&mut self, mut routine: F) 
	where F: FnMut(&mut RenderWindow) -> ()
	{
		while self.window.is_open() {
			while let Some(event) = self.window.poll_event() {
				if event == Event::Closed {
					self.window.close();
				}
			}

			self.window.set_active(true);
			routine(&mut self.window);
		}
	}	
}