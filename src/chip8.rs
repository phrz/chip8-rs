
// CHIP-8 Memory Map
// 0x000-0x1FF - Chip 8 interpreter (contains font set in emu)
// 0x050-0x0A0 - Used for the built in 4x5 pixel font set (0-F)
// 0x200-0xFFF - Program ROM and work RAM
//
// OPCODES
//
// these two can set the draw flag
// 0x00E0 - clear screen
// 0xDXYN - draw a sprite

extern crate rand;

use chip8::rand::prelude::*;

pub const CHIP8_STACK_SIZE: u16 = 16;

pub const CHIP8_FONT_BASE_ADDRESS: u16 = 0x50;
pub const CHIP8_PROGRAM_BASE_ADDRESS: u16 = 0x200;

pub const CHIP8_DISPLAY_WIDTH: usize = 64;
pub const CHIP8_DISPLAY_HEIGHT: usize = 32;

pub struct Chip8 {

	// 16-bit opcodes
	opcode: u16,

	// 4 kibibytes of memory
	memory: [u8; 4096],

	// v0,v1...vE (general purpose)
	// vF [#16] (carry flag)
	registers: [u8; CHIP8_STACK_SIZE as usize],

	// 0x000-0xFFF
	index_register: u16,
	program_counter: u16,

	// a simple stack that stores the program counter
	// when subroutine opcodes (like jump) are called,
	// allowing us to restore program state.
	stack: [u16; 16],
	stack_pointer: u16,

	// two timers that count down to zero when set
	// to nonzero. Sound timer rings buzzer when
	// it reaches zero.
	delay_timer: u8,
	sound_timer: u8,

	// CHIP-8 has a hexadecimal keypad (0x0...0xF)
	// store the keypad state
	keypad_state: [bool; 16],

	// 2048 black and white pixels (64 x 32 = 2048)
	// ROW MAJOR
	screen: [[bool; CHIP8_DISPLAY_WIDTH]; CHIP8_DISPLAY_HEIGHT],

	_is_beeping: bool,
}

impl Chip8 {
	pub fn new(program_bytes: &[u8]) -> Chip8 {
		let mut chip8 = Chip8 {
			opcode: 0,
			memory: [0u8; 4096],
			registers: [0u8; 16],

			index_register: 0,
			program_counter: CHIP8_PROGRAM_BASE_ADDRESS, // where code begins

			stack: [0u16; CHIP8_STACK_SIZE as usize],
			// points to empty space, zero = empty stack
			stack_pointer: 0, 

			delay_timer: 0,
			sound_timer: 0,

			keypad_state: [false; 16],
			screen: [[false; CHIP8_DISPLAY_WIDTH]; CHIP8_DISPLAY_HEIGHT],

			_is_beeping: false,
		};

		// Load fontset at 0x50
		// [CITE] http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#font

		let font_bytes: [u8; 80] = [
			0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
			0x20, 0x60, 0x20, 0x20, 0x70, // 1
			0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
			0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
			0x90, 0x90, 0xF0, 0x10, 0x10, // 4
			0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
			0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
			0xF0, 0x10, 0x20, 0x40, 0x40, // 7
			0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
			0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
			0xF0, 0x90, 0xF0, 0x90, 0x90, // A
			0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
			0xF0, 0x80, 0x80, 0x80, 0xF0, // C
			0xE0, 0x90, 0x90, 0x90, 0xE0, // D
			0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
			0xF0, 0x80, 0xF0, 0x80, 0x80, // F
		];

		for i in 0usize..80 {
			chip8.memory[CHIP8_FONT_BASE_ADDRESS as usize + i] = font_bytes[i];
		}


		// Load program at 0x200
		for i in 0..program_bytes.len() {
			chip8.memory[CHIP8_PROGRAM_BASE_ADDRESS as usize + i] = program_bytes[i];
		}

		chip8
	}

	pub fn is_beeping(&self) -> bool {
		self._is_beeping
	}

	pub fn set_key_pressed(&mut self, key: u8, is_pressed: bool) {
		if key > 0xF {
			panic!("Attempted to set key greater than 0xF in set_key_pressed.");
		}
		self.keypad_state[key as usize] = is_pressed;
	}

	pub fn get_screen(&self) -> &[[bool; CHIP8_DISPLAY_WIDTH]; CHIP8_DISPLAY_HEIGHT] {
		return &self.screen;
	}

	fn increment_program_counter(&mut self) {
		// increment by 2 because program counter
		// is in bytes (8 bits), but opcodes are 16 bit.
		let (result, overflow) = self.program_counter.overflowing_add(2);
		if overflow {
			panic!("Program counter overflowed on increment.");
		}
		self.program_counter = result;
	}

	fn skip_program_counter(&mut self) {
		// increment by 4 bytes to move past this instruction
		// and skip the next (required in many opcodes)
		let (result, overflow) = self.program_counter.overflowing_add(4);
		if overflow {
			panic!("Program counter overflowed on increment.");
		}
		self.program_counter = result;
	}

	fn draw_sprite(&mut self, x: usize, y: usize, n: u8) {
		// Begin with the assumption there is no pixel flipping
		self.registers[0xF] = 0;

		let vx: u8 = self.registers[x];
		let vy: u8 = self.registers[y];

		if vx as usize > CHIP8_DISPLAY_WIDTH - 1 {
			return;
			// panic!("x exceeds screen width coordinates in draw command.");
		}

		if vy as usize > CHIP8_DISPLAY_HEIGHT - 1 {
			return;
			// panic!("y exceeds screen height coordinates in draw command.");
		}

		for row in 0u8..n {
			// get the 8 bit integer from memory
			// at index_register + row representing one row of the sprite.
			let mem_i: usize = (self.index_register + row as u16) as usize;
			let memory_row: u8 = self.memory[mem_i];

			let row_i: usize = row as usize + vy as usize;

			// don't write row if it's offscreen (undefined behavior)
			if row_i > CHIP8_DISPLAY_HEIGHT - 1 {
				//println!("Warning: row out of bounds ({} on a screen of height {})", row_i, CHIP8_DISPLAY_HEIGHT);
				continue;
			}

			for col in 0u8..8 {
				// write the single bit to the screen matrix
				let col_i: usize = col as usize + vx as usize;

				if col_i > CHIP8_DISPLAY_WIDTH - 1 {
					/*println!("Warning: column out of bounds ({} on a screen of width {})", col_i, CHIP8_DISPLAY_WIDTH);
					println!("0x{:X} draw to x:{}, y:{}", self.opcode, x, y);
					println!("Current row in sprite: {}", row);
					println!("Current col in sprite: {}", col);*/
					continue;
				}

				let old_screen_pixel: bool = self.screen[row_i][col_i];
				let sprite_pixel: bool = ((memory_row >> (7-col)) & 1) == 1;

				// with XOR, the only way to flip the screen from 1 to 0
				// is if the screen and sprite pixels are both 1.
				// We need to detect this to set VF.
				if old_screen_pixel && sprite_pixel {
					self.registers[0xF] = 1;
				}

				// XOR drawing mode
				self.screen[row_i][col_i] = old_screen_pixel ^ sprite_pixel;
			}
		}
	}

	// opcodes are stored big-endian
	pub fn run_cycle(&mut self) {

		self._is_beeping = false;

		// fetch
		// -- get a 16b opcode from the memory location
		//    defined by the program counter (pc)
		self.opcode = 
			(self.memory[self.program_counter as usize] as u16) << 8 |
			(self.memory[(self.program_counter + 1) as usize] as u16);

		//print!("pc: 0x{:01$X}   ", self.program_counter, 4);
		//println!("op: 0x{:01$X}", self.opcode, 4);

		// decode
		// all opcodes begin with 4 instruction bits
		// instruction: highest 4 bits
		let inst: u16 = (self.opcode & 0xF000) >> 12;
		
		// NNN
		// some opcodes contain a 12 bit address constant
		// at the right (0x000..0xFFF), up to 4095
		let nnn: u16 = self.opcode & 0x0FFF;

		// NN
		// some opcodes have an 8b constant at the right 
		// (0x00..0xFF)
		let nn: u8 = (self.opcode & 0xFF) as u8;

		// N
		// some opcodes contain 4b constants at the right
		// (0x0..0xF)
		let n: u8 = (self.opcode & 0xF) as u8;

		// X and Y
		// some opcodes contain one or two register identifiers,
		// each four bits, bits laid out like this:
		//
		// _ _ _ _  X X X X  Y Y Y Y  _ _ _ _
		//
		let x: usize = ((self.opcode & 0x0F00) >> 8) as usize;
		let y: usize = ((self.opcode & 0x00F0) >> 4) as usize;

		// execute
		// -- for example, run `mvi` (set Index register)
		//    command by dropping the command code (first four bits)
		//    and using the remaining 12 bits as the value to set
		//
		// I = opcode & 0x0FFF;
		// pc += 2;
		
		// 0NNN: call RCA 1802 program at NNN.
		if inst == 0 && x != 0 && n != 0 {
			panic!("Warning: RCA 1802 not implemented.");
		}
		// 00E0: clear display
		else if self.opcode == 0x00E0u16 {
			self.screen = [[false; CHIP8_DISPLAY_WIDTH]; CHIP8_DISPLAY_HEIGHT];
			self.increment_program_counter();
		}
		// 00EE: return from subroutine
		else if self.opcode == 0x00EEu16 {
			//println!("Return from subroutine");
			//println!("sp: {}    stack: {:?}", self.stack_pointer, self.stack);

			if self.stack_pointer == 0 {
				panic!("Tried to return from subroutine with empty stack (meaning not in a subroutine)");
			}
			// since the stack pointer points PAST the last stack entry, subtract one
			self.program_counter = self.stack[self.stack_pointer as usize - 1];
			self.stack_pointer -= 1; // show that the space we just consumed is free
		}
		// 1NNN: unconditionally jump to NNN
		else if inst == 0x1 {
			self.program_counter = nnn;
		}
		// 2NNN: call subroutine at NNN
		else if inst == 0x2 {
			// no free space
			if self.stack_pointer == CHIP8_STACK_SIZE {
				panic!("Stack overflow.");
			}
			self.increment_program_counter();
			self.stack[self.stack_pointer as usize] = self.program_counter;
			self.stack_pointer += 1;
			self.program_counter = nnn;
		}
		// 3XNN: *skip* the next instruction
		// if (Vx == NN).
		else if inst == 0x3 {
			let vx: u8 = self.registers[x];
			if vx == nn {
				self.skip_program_counter();
			} else {
				self.increment_program_counter();
			}
		}
		// 4XNN: *skip* the next instruction
		// if (Vx != NN).
		else if inst == 0x4 {
			let vx: u8 = self.registers[x];
			if vx != nn {
				self.skip_program_counter();
			} else {
				self.increment_program_counter();
			}
		}
		// 5XY0: *skip* the next instruction
		// if (Vx == Vy).
		else if inst == 0x5 {
			let vx: u8 = self.registers[x];
			let vy: u8 = self.registers[y];
			if vx == vy {
				self.skip_program_counter();
			} else {
				self.increment_program_counter();
			}
		}
		// 6XNN: Set Vx := NN.
		else if inst == 0x6 {
			self.registers[x] = nn;
			self.increment_program_counter();
		}
		// 7XNN: Vx += NN, no carry flag.
		else if inst == 0x7 {
			let vx: u8 = self.registers[x];
			self.registers[x] = vx.wrapping_add(nn);
			self.increment_program_counter();
		}
		// 8XY0: Vx := Vy
		else if inst == 0x8 && n == 0x0 {
			let vy: u8 = self.registers[y];
			self.registers[x] = vy;
			self.increment_program_counter();
		}
		// 8XY1: Vx := Vx | Vy (Bitwise OR)
		else if inst == 0x8 && n == 0x1 {
			let vx: u8 = self.registers[x];
			let vy: u8 = self.registers[y];
			self.registers[x] = vx | vy;
			self.increment_program_counter();
		}
		// 8XY2: Vx := Vx & Vy (Bitwise AND)
		else if inst == 0x8 && n == 0x2 {
			let vx: u8 = self.registers[x];
			let vy: u8 = self.registers[y];
			self.registers[x] = vx & vy;
			self.increment_program_counter();
		}
		// 8XY3: Vx := Vx ^ Vy (Bitwise XOR)
		else if inst == 0x8 && n == 0x3 {
			let vx: u8 = self.registers[x];
			let vy: u8 = self.registers[y];
			self.registers[x] = vx ^ vy;
			self.increment_program_counter();
		}
		// 8XY4: Vx += Vy with VF carry flag. 1=carry, 0=none
		else if inst == 0x8 && n == 0x4 {
			let vx: u8 = self.registers[x];
			let vy: u8 = self.registers[y];
			let (result, carry) = vx.overflowing_add(vy);

			self.registers[x] = result;
			self.registers[0xF] = if carry { 1 } else { 0 };

			self.increment_program_counter();
		}
		// 8XY5: Vx -= Vy with VF borrow flag, 0=borrow, 1=none
		else if inst == 0x8 && n == 0x5 {
			let vx: u8 = self.registers[x];
			let vy: u8 = self.registers[y];
			// WARNING: Assuming borrow and carry are equivalent
			let (result, borrow) = vx.overflowing_sub(vy);

			self.registers[x] = result;
			self.registers[0xF] = if borrow { 0 } else { 1 };

			self.increment_program_counter();
		}
		// 8XY6: Vx := Vy >> 1. VF := Vy[0] (least significant before)
		else if inst == 0x8 && n == 0x6 {
			let vy: u8 = self.registers[y];

			self.registers[0xF] = vy & 0x1;
			self.registers[x] = vy >> 1;

			self.increment_program_counter();
		}
		// 8XY7: Vx := Vy - Vx with VF borrow flag, 0=borrow, 1=none
		else if inst == 0x8 && n == 0x7 {
			let vx: u8 = self.registers[x];
			let vy: u8 = self.registers[y];

			let (result, borrow) = vy.overflowing_sub(vx);

			self.registers[x] = result;
			self.registers[0xF] = if borrow { 0 } else { 1 };

			self.increment_program_counter();
		}
		// 8XYE: Vy := Vy << 1; Vx := Vy; VF = Vy[7] (msb)	
		else if inst == 0x8 && n == 0xE {
			let vy: u8 = self.registers[y];
			let result = vy << 1;

			self.registers[0xF] = (vy >> 7) & 1;
			self.registers[y] = result;
			self.registers[x] = result;

			self.increment_program_counter();
		}
		// 9XY0: *skip* the next instruction
		// if (Vx != Vy).
		else if inst == 0x9 && n == 0x0 {
			let vx: u8 = self.registers[x];
			let vy: u8 = self.registers[y];
			if vx != vy {
				self.skip_program_counter();
			} else {
				self.increment_program_counter();
			}
		}
		// ANNN: I := NNN
		else if inst == 0xA {
			self.index_register = nnn;
			self.increment_program_counter();
		}
		// BNNN: PC := V0 + NNN
		else if inst == 0xB {
			let (result, overflow) = (self.registers[0] as u16).overflowing_add(nnn);
			if overflow {
				panic!("Overflow on PC := V0 + NNN opcode [0xB___]");
			}
			self.program_counter = result;
		}
		// CXNN: Vx := rand(255) & NN
		else if inst == 0xC {
			let base: u8 = random(); // 0-255
			self.registers[x] = base & nn;
			self.increment_program_counter();
		}
		// DXYN: draw(Vx,Vy,N)
		// a sprite at (*Vx,*Vy) 8 pixels wide, N pixels tall.
		// each 8-pixel row is read sequentially beginning at
		// memory[I], where I = index reg. This does not mutate I.
		// VF := 1 if any pixels are flipped from set to unset.
		// Sprite pixels are XOR'd with screen pixels.
		else if inst == 0xD {
			self.draw_sprite(x,y,n);
			self.increment_program_counter();
		}
		// EX9E: *skip* the next instruction
		// if (keys[Vx].is_key_down)
		else if inst == 0xE && nn == 0x9E {
			let vx: u8 = self.registers[x];
			if vx > 0xF { // keypad size
				panic!("Keypad state requested for key index larger than 0xF!");
			}

			if self.keypad_state[vx as usize] {
				self.skip_program_counter();
			} else {
				self.increment_program_counter();
			}
		}
		// EXA1: *skip* the next instruction
		// if (not keys[Vx].is_key_down)
		else if inst == 0xE && nn == 0xA1 {
			let vx: u8 = self.registers[x];
			if vx > 0xF { // keypad size
				panic!("Keypad state requested for key index larger than 0xF!");
			}

			if !self.keypad_state[vx as usize] {
				self.skip_program_counter();
			} else {
				self.increment_program_counter();
			}
		}
		// FX07: Vx := delay_timer
		else if inst == 0xF && nn == 0x07 {
			self.registers[x] = self.delay_timer;
			self.increment_program_counter();
		}
		// FX0A: Block until keydown, then Vx := key_code.
		else if inst == 0xF && nn == 0x0A {
			// do NOT increment pc, do NOT busy wait, instead,
			// cycle without incrementing PC so input can be updated
			let vx: u8 = self.registers[x];
			if vx > 0xF { // keypad size
				panic!("Keypad state requested for key index larger than 0xF!");
			}

			if self.keypad_state[vx as usize] {
				self.increment_program_counter();
			}
			// otherwise cycle to allow this command to execute again
		}
		// FX15: delay_timer := Vx
		else if inst == 0xF && nn == 0x15 {
			self.delay_timer = self.registers[x];
			self.increment_program_counter();
		}
		// FX18: sound_timer := Vx
		else if inst == 0xF && nn == 0x18 {
			self.sound_timer = self.registers[x];
			self.increment_program_counter();
		}
		// FX1E: I += Vx
		else if inst == 0xF && nn == 0x1E {
			self.index_register += self.registers[x] as u16;
			self.increment_program_counter();
		}
		// FX29: I := font_address[Vx]
		// Sets I to the location of the sprite for the 
		// character in Vx. Characters 0-F (in hexadecimal) 
		// are represented by a 4x5 font.
		else if inst == 0xF && nn == 0x29 {
			let vx: u8 = self.registers[x];
			if vx > 0xF {
				panic!("Attempted to get font address for value > 0xF");
			}
			// fonts are stored at base address.
			// Each character is 5 bytes.
			// Characters are stored in hex order 0...9,A...F
			self.index_register = CHIP8_FONT_BASE_ADDRESS + (vx as u16) * 5;

			self.increment_program_counter();
		}
		// FX33: Binary coded decimal conversion.
		// memory[I] := bcd_hundreds(Vx)
		// memory[I+1] := bcd_tens(Vx)
		// memory[I+2] := bcd_ones(Vx)
		else if inst == 0xF && nn == 0x33 {
			let vx = self.registers[x];

			let i = self.index_register as usize;
			self.memory[i + 0] = vx / 100;
			self.memory[i + 1] = (vx / 10 ) % 10;
			self.memory[i + 2] = (vx % 100) % 10;
			
			self.increment_program_counter();
		}
		// FX55: STORE register(s) V0...Vx (inclusive)
		// in memory beginning at memory[I]. I (index reg)
		// *is mutated* by 1 for each value stored.
		else if inst == 0xF && nn == 0x55 {
			for r in 0usize..x+1 {
				self.memory[self.index_register as usize] = self.registers[r];
				self.index_register += 1;
			}
			self.increment_program_counter();
		}
		// FX65: LOAD memory to registers V0...Vx (inclusive)
		// from memory beginning at memory[I]. I (index reg)
		// *is mutated* by 1 for each value loaded.
		else if inst == 0xF && nn == 0x65 {
			if x == 0xF {
				panic!("Attempted to load into non-general-purpose register VF.");
			}
			for r in 0usize..x+1 {
				self.registers[r] = self.memory[self.index_register as usize];
				self.index_register += 1;
			}
			self.increment_program_counter();
		}
		// bad opcode
		else {
			println!("Unknown opcode: 0x{:01$X}", self.opcode, 4);
			println!("At program counter: 0x{:01$X}", self.program_counter, 4);
			panic!("Unknown opcode, see above");
		}
		// ===============================================
		// end opcode execution

		// update timers
		// these timers count down at 60 Hz
		// (ASSUMING 60Hz operation)
		let sound_before: bool = self.sound_timer != 0;

		self.delay_timer = self.delay_timer.saturating_sub(1);
		self.sound_timer = self.sound_timer.saturating_sub(1);

		if sound_before && self.sound_timer == 0 {
			self._is_beeping = true;
		}
	}
}