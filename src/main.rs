extern crate sfml;
use sfml::graphics::{ Color, RenderTarget, Texture, Sprite, Transformable };
use sfml::system::Vector2f;
use sfml::window::Key;
use sfml::audio::{ Sound, SoundBuffer };

mod graphics_state;
use graphics_state::GraphicsState;

mod chip8;
use chip8::Chip8;

use std::io::prelude::*;
use std::fs::File;

use std::env;
use std::process;

fn screen_to_texture(emulator: &Chip8) -> Texture {
	let mut texture = Texture::new(
		chip8::CHIP8_DISPLAY_WIDTH as u32, 
		chip8::CHIP8_DISPLAY_HEIGHT as u32
	).unwrap();

	let mut pixels = [0u8; chip8::CHIP8_DISPLAY_WIDTH * chip8::CHIP8_DISPLAY_HEIGHT * 4];
	let screen = emulator.get_screen();

	for r in 0..screen.iter().len() {
		for c in 0..screen[r].iter().len() {
			let color: u8 = if screen[r][c] { 255 } else { 0 };
			let i = (r * chip8::CHIP8_DISPLAY_WIDTH + c) * 4;
			pixels[i] = color;
			pixels[i+1] = color;
			pixels[i+2] = color;
			pixels[i+3] = 255;
		}
	}

	texture.update_from_pixels(
		&pixels, 
		chip8::CHIP8_DISPLAY_WIDTH as u32,
		chip8::CHIP8_DISPLAY_HEIGHT as u32,
		0, // x
		0, // y
	);

	texture
}

fn make_beep() -> SoundBuffer {
	const SAMPLE_RATE: u32 = 22050;
	const N_SAMPLES: usize = (SAMPLE_RATE / 3) as usize; // 2 seconds

	let mut samples = [0i16; N_SAMPLES];
	let mut x = 0i16;

	// triangle wave
	// 440Hz * 22050Hz / 65535 = x
	let step: i16 = (800.0 * SAMPLE_RATE as f64 / 65535.0) as i16;
	for i in 0..N_SAMPLES {
		samples[i] = x;
		x = x.wrapping_add(step);
	}

	SoundBuffer::from_samples(&samples, 1, SAMPLE_RATE).unwrap()
}

fn main() -> std::io::Result<()> {

	let args: Vec<_> = env::args().collect();

	if args.len() != 2 {
		println!("Please pass the program name as an argument");
		process::exit(1);
	}

	let beep = make_beep();
	let mut sound = Sound::with_buffer(&beep);

	let mut f = File::open(args[1].to_string())?;
	let mut program_bytes: Vec<u8> = Vec::new();
	f.read_to_end(&mut program_bytes)?;

	let mut emulator = Chip8::new(&program_bytes);

	let scale_factor = 20u32;
	let w: u32 = chip8::CHIP8_DISPLAY_WIDTH as u32 * scale_factor;
	let h: u32 = chip8::CHIP8_DISPLAY_HEIGHT as u32 * scale_factor;

	let mut graphics = GraphicsState::new(w,h);

	// Keyboard layout
	//
	// 1 2 3 4
	// q w e r
	// a s d f
	// z x c v
	//
	// maps to:
	//
	// 1 2 3 C
	// 4 5 6 D
	// 7 8 9 E
	// A 0 B F
	//
	let keyboard: [Key; 16] = [
		Key::Num1, Key::Num2, Key::Num3, Key::Num4,
		Key::Q,    Key::W,    Key::E,    Key::R,
		Key::A,    Key::S,    Key::D,    Key::F,
		Key::Z,    Key::X,    Key::C,    Key::V,
	];

	let chip8_keypad: [u8; 16] = [
		0x1, 0x2, 0x3, 0xC,
		0x4, 0x5, 0x6, 0xD,
		0x7, 0x8, 0x9, 0xE,
		0xA, 0x0, 0xB, 0xF,
	];

	// run the render loop
	graphics.render_loop(|window| {
		for i in 0x0..0xF {
			let keyboard_code = keyboard[i];
			let keypad_code = chip8_keypad[i];

			emulator.set_key_pressed(keypad_code, keyboard_code.is_pressed());
		}

		if emulator.is_beeping() {
			sound.play();
		}

		window.clear(&Color::rgb(0,0,0));
		
		let tex = screen_to_texture(&emulator);
		let mut sprite = Sprite::with_texture(&tex);
		let sf = scale_factor as f32;
		sprite.set_scale(Vector2f::new(sf,sf));
		window.draw(&sprite);

		window.display();

		emulator.run_cycle();
	});

	Ok(())
}
